<div align="center">
	<a href="https://forgejo.org">
		<img src="https://gitlab.com/uploads/-/system/group/avatar/64459497/73144-c883a242dec5299fbc06bbe3ee71d8c6.png?width=128" alt="" width="128" align="center" />
		<h1 align="center">Forgejo - Beyond coding. We forge.</h1>
	</a>
</div>

You've found us - well, sort of...

We are forging Forgejo using Forgejo as our software forge, so you can find us over at <a href="https://codeberg.org/forgejo" rel="me canonical">https://codeberg.org/forgejo</a>.

More information, FAQs, links to socials, etc can be found on [our website](https://forgejo.org/).

See you over there!

<div align="center">
	<a href="https://codeberg.org/forgejo">
		<img src="https://gitlab.com/forgejo/gitlab-profile/-/raw/main/assets/codeberg-badge.svg?ref_type=heads" width="192" />
	</a>
</div>
